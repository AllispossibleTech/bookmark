/* Select * from author; */

/*
ALTER TABLE preferred_genre
ALTER COLUMN preferred_genre int NOT NULL;
*/


/*INSERT INTO book (isbn, author_id, title, description, publish_date, page_number, average_rating, total_ratings, ratings_nr, genre)
VALUES ("0285473857462", 1, "Nice book", "nice indeed", 2022-09-28, 50, 0, 0, 0, "Mystery");
*/

/*
SELECT *
FROM author
INNER JOIN book ON author.author_id = book.author_id;
*/

/* SELECT * FROM read_book; */

/*alter table book 
drop constraint [PK_book]

--------------------------------------------------------------------------
*/

/*SELECT * FROM readers
RIGHT JOIN users ON readers.reader_id = users.id;
*/

/*
DROP TABLE books;
DROP TABLE authors;
DROP TABLE comments;
DROP TABLE preferred_genres;
DROP TABLE read_books;
DROP TABLE readers;
*/

/*INSERT INTO authors ([name], surname, email, [password], country) VALUES('giacomo', 'mattiuzzo', 'giacomo@gmail.com', 'coolpsw', 'IT') */


/*	RETRIEVE JUST INSERTED FIRST COLUMN ID CODE

INSERT INTO readers (name, surname, email, password, country, preferred_page_nr, vote_nr) 
OUTPUT INSERTED.reader_id
VALUES('marco', 'rota', 'marco@gmail.com', 'secretpsw', 'IT', 150, 0); */


/* SCRIPT USED TO CREATE THE TABLES DOWN HERE! */

/*CREATE SEQUENCE UsersIdCounter
AS int
START WITH 1  
INCREMENT BY 1;


CREATE TABLE authors(
author_id int PRIMARY KEY CLUSTERED DEFAULT (NEXT VALUE FOR UsersIdCounter),
[name] nchar (50) NOT NULL,
surname nchar (50) NOT NULL,
email nchar (50) NOT NULL,
[password] nchar (50) NOT NULL,
country nchar (2) NOT NULL,
)

CREATE TABLE books(
isbn nchar (13) NOT NULL primary key,
author_id int NOT NULL,
title nchar (50) NOT NULL,
[description] text NOT NULL,
publish_date datetime NOT NULL,
page_number int NOT NULL,
total_ratings int NOT NULL,
ratings_nr int NOT NULL,
genre int NOT NULL,
CONSTRAINT FK_bookAuthor FOREIGN KEY (author_id) REFERENCES authors(author_id)
)

CREATE TABLE readers(
reader_id int PRIMARY KEY CLUSTERED DEFAULT (NEXT VALUE FOR UsersIdCounter),
[name] nchar (50) NOT NULL,
surname nchar (50) NOT NULL,
email nchar (50) NOT NULL,
[password] nchar (50) NOT NULL,
country nchar (2) NOT NULL,
preferred_page_nr int NOT NULL,
vote_nr int NOT NULL,
)

CREATE TABLE comments(
isbn nchar (13) NOT NULL,
reader_id int NOT NULL,
comment_text text NOT NULL,
[date] datetime NOT NULL,
CONSTRAINT FK2_commentBook FOREIGN KEY (isbn) REFERENCES books(isbn),
CONSTRAINT FK3_commentReader FOREIGN KEY (reader_id) REFERENCES readers(reader_id),
)

CREATE TABLE preferred_genres(
reader_id int NOT NULL,
preferred_genre int NOT NULL,
CONSTRAINT FK4_preferredGenre FOREIGN KEY (reader_id) REFERENCES readers(reader_id),
)

CREATE TABLE read_books(
reader_id int NOT NULL,
isbn nchar(13) NOT NULL,
CONSTRAINT FK5_readBookReader FOREIGN KEY (reader_id) REFERENCES readers(reader_id),
CONSTRAINT FK6_readBookIsbn FOREIGN KEY (isbn) REFERENCES books(isbn),
)
*/

/* END OF SCRIPT USED TO CREATE THE TABLES */

