﻿using Microsoft.VisualBasic;
using System.Data.SqlClient;
using DomainClassLibrary;

namespace DataAccessClassLibrary
{
    public class UserRepository : IUserRepository
    {
        private const string connectionString = "Server=mssqlstud.fhict.local;Database=dbi501938;User Id = dbi501938; Password=alessandrosqlfontys;";

        // METHOD THAT RETURNS A LIST OF ALL USERS (AUTHORS AND READERS)

        public List<User> GetAll()
        {
            List<User> result = new List<User>();

            foreach(Reader r in GetReaders())
            {
                result.Add(r);
            }

            foreach (Author a in GetAuthors())
            {
                result.Add(a);
            }
            
            return result;
        }

        public User GetUser(int id)
        {
            foreach (User r in GetAll())
            {
                if (r.Id == id)
                {
                    return r;
                }
            }
            return null;
        }

        // METHOD THAT RETURNS A LIST OF AUTHORS WITH THEIR BOOKS INSIDE (USED BY GetAll())

        public List<Author> GetAuthors()
        {
            List<Author> result = new List<Author>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT * FROM authors";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();


                    while (reader.Read())
                    {
                        result.Add(new Author(reader[1].ToString().Trim(), reader[2].ToString().Trim(), reader[3].ToString().Trim(), reader[4].ToString().Trim(), (string)reader[5], (int)reader[0]));
                    }

                    foreach (Author a in result)
                    {
                        foreach (Book b in GetBooksByAuthor(a))
                        {
                            a.PublishedBooks.Add(b);
                        }
                    }
                    return result;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return null;
        }

        public Author GetAuthorById(int id)
        {
            foreach (Author a in GetAuthors())
            {
                if (a.Id == id)
                {
                    return a;
                }
            }
            return null;
        }
        
        
        // METHOD THAT RETURNS THE LIST OF BOOKS THAT A SPECIFIC AUTHOR HAS PUBLISHED
        
        public List<Book> GetBooksByAuthor(Author a)
        {
            List<Book> result = new List<Book>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT isbn, author_id, title, description, publish_date, page_number, total_ratings, ratings_nr, genre FROM books WHERE author_id=@author_id";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    command.Parameters.AddWithValue("@author_id", a.Id);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new Book(((string)reader[0]).Trim(), (string)reader[2], a, (string)reader[3], DateTime.Parse(reader[4].ToString()), (Genre)reader[8], (int)reader[5], (int)reader[6], (int)reader[7])); // ENUMS ARE STORED AS NUMBERS
                    }
                }

                foreach (Book b in result)
                {
                    foreach (Comment c in GetCommentsByIsbn(b.Isbn))
                    {
                        b.Comments.Add(c);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return null;
        }

        // METHOD THAT RETURNS A LIST OF COMMENTS OF A SPECIFIC BOOK
        
        public List<Comment> GetCommentsByIsbn(string isbn)
        {
            List<Comment> result = new List<Comment>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT * FROM comments WHERE isbn=@isbn";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    command.Parameters.AddWithValue("@isbn", isbn);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new Comment(GetReader((int)reader[1]), (string)reader[2], (DateTime)reader[3]));
                    }
                }
                return result;
            }
            catch(Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return null;
        }

        public Reader GetReader(int id)
        {
            foreach (Reader r in this.GetReaders())
            {
                if (r.Id == id)
                {
                    return r;
                }
            }
            return null;
        }

        // METHOD THAT RETURNS A LIST OF READERS (USED BY GetAll())
        public List<Reader> GetReaders()
        {
            List<Reader> result = new List<Reader>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT * FROM readers";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new Reader(reader[1].ToString().Trim(), reader[2].ToString().Trim(), reader[3].ToString().Trim(), reader[4].ToString().Trim(), (string)reader[5], (int)reader[0], (int)reader[6], (int)reader[7]));
                    }
                }

                foreach (Reader r in result)
                {
                    foreach (Book b in GetReadBooksByReaderId(r.Id))
                    {
                        r.ReadBooks.Add(b);
                    }
                }

                foreach (Reader r in result)
                {
                    foreach (Genre g in GetPreferredGenresByReaderId(r.Id))
                    {
                        r.LovedGenres.Add(g);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return null;
        }

        // METHOD THAT RETURNS A LIST OF GENRES PREFERRED BY A SPECIFIC READER
        public List<Genre> GetPreferredGenresByReaderId(int id)
        {
            List<Genre> result = new List<Genre>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT preferred_genre FROM preferred_genres WHERE reader_id=@reader_id";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    command.Parameters.AddWithValue("@reader_id", id);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add((Genre)reader[0]);
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());            
            }
            return null;
        }

        // METHOD THAT RETURNS THE LIST OF BOOKS READ BY A SPECIFIC READER
        public List<Book> GetReadBooksByReaderId(int id)
        {
            List<Book> result = new List<Book>();
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "SELECT b.* FROM read_books AS rb INNER JOIN books AS b ON b.isbn=rb.isbn WHERE rb.reader_id=@reader_id";
                    SqlCommand command = new SqlCommand(sql, connection);
                    connection.Open();
                    command.Parameters.AddWithValue("@reader_id", id);
                    SqlDataReader reader = command.ExecuteReader();

                    while (reader.Read())
                    {
                        result.Add(new Book(((string)reader[0]).Trim(), (string)reader[2], GetAuthorById((int)reader[1]), (string)reader[3], (DateTime)reader[4], (Genre)reader[8], (int)reader[5], (int)reader[6], (int)reader[7])); // ENUMS ARE STORED AS NUMBERS
                    }
                }
                return result;
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
            return null;
        }


        public void AddBook(Book b)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "INSERT INTO books (isbn, author_id, title, description, publish_date, page_number, total_ratings, ratings_nr, genre) VALUES(@isbn, @author_id, @title, @description, @publish_date, @page_number, @total_ratings, @ratings_nr, @genre)";
                    SqlCommand command = new SqlCommand(sql, connection);

                    connection.Open();

                    command.Parameters.AddWithValue("@isbn", b.Isbn);
                    command.Parameters.AddWithValue("@author_id", b.Author.Id);
                    command.Parameters.AddWithValue("@title", b.Title);
                    command.Parameters.AddWithValue("@description", b.Description);
                    command.Parameters.AddWithValue("@publish_date", b.PublishDate);
                    command.Parameters.AddWithValue("@page_number", b.PageNumber);
                    command.Parameters.AddWithValue("@total_ratings", 0);
                    command.Parameters.AddWithValue("@ratings_nr", 0);
                    command.Parameters.AddWithValue("@genre", (int)b.Genre);

                    command.ExecuteNonQuery();
                }
            }
            catch(Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
        }

        // METHOD THAT UPDATES THE NUMBER OF VOTES THAT A READER HAS

        public void AddVote(int id, int voteNr)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "UPDATE readers SET vote_nr = @vote_nr WHERE reader_id=@id";
                    SqlCommand command = new SqlCommand(sql, connection);

                    connection.Open();

                    command.Parameters.AddWithValue("@vote_nr", voteNr);
                    command.Parameters.AddWithValue("@id", id);

                    command.ExecuteNonQuery();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
        }

        // METHOD THAT UPDATES THE RATING STATS. OF A BOOK

        public void AddRating(Book b)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "UPDATE books SET total_ratings=@total_ratings, ratings_nr=@ratings_nr WHERE isbn=@isbn";
                    SqlCommand command = new SqlCommand(sql, connection);

                    connection.Open();

                    command.Parameters.AddWithValue("@total_ratings", b.TotalRatings);
                    command.Parameters.AddWithValue("@ratings_nr", b.NrOfRatings);
                    command.Parameters.AddWithValue("@isbn", b.Isbn);

                    command.ExecuteNonQuery();

                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
        }

        // METHOD THAT CHECKS WHETHER AN USER EXISTS

        public bool Exists(int id)
        {
            foreach(User u in GetAll())
            {
                if(u.Id == id)
                {
                    return true;
                }
            }
            return false;
        }

        // METHOD THAT ADDS AN USER

        public void Add(User user)
        {
            if (user is Author)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        string sql = "INSERT INTO authors (name, surname, email, password, country) VALUES(@name, @surname, @email, @password, @country)";
                        SqlCommand command = new SqlCommand(sql, connection);

                        connection.Open();

                        command.Parameters.AddWithValue("@name", user.Name);
                        command.Parameters.AddWithValue("@surname", user.Surname);
                        command.Parameters.AddWithValue("@email", user.Email);
                        command.Parameters.AddWithValue("@password", user.Password);
                        command.Parameters.AddWithValue("@country", user.Country);

                        command.ExecuteNonQuery();

                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.ToString());
                }
            } else if (user is Reader)
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(connectionString))
                    {
                        string sql = "INSERT INTO readers (name, surname, email, password, country, preferred_page_nr, vote_nr) OUTPUT INSERTED.reader_id VALUES (@name, @surname, @email, @password, @country, @preferred_page_nr, @vote_nr);";
                        SqlCommand command = new SqlCommand(sql, connection);

                        connection.Open();

                        command.Parameters.AddWithValue("@name", user.Name);
                        command.Parameters.AddWithValue("@surname", user.Surname);
                        command.Parameters.AddWithValue("@email", user.Email);
                        command.Parameters.AddWithValue("@password", user.Password);
                        command.Parameters.AddWithValue("@country", user.Country);
                        command.Parameters.AddWithValue("@preferred_page_nr", ((Reader)user).PreferredNumberOfPages);
                        command.Parameters.AddWithValue("@vote_nr", 0);

                        int newID = (int)command.ExecuteScalar();
                        user.Id = newID;

                    }

                    foreach (Genre g in ((Reader)user).LovedGenres)
                    {
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            string sql = "INSERT INTO preferred_genres (reader_id, preferred_genre) VALUES(@reader_id, @preferred_genre)";
                            SqlCommand command = new SqlCommand(sql, connection);

                            connection.Open();

                            command.Parameters.AddWithValue("@reader_id", user.Id);
                            command.Parameters.AddWithValue("@preferred_genre", (int)g);

                            command.ExecuteNonQuery();
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception($"There was an error while adding the new Reader to the Database: {Ex}");
                }
            }
        }

        public void AddComment(Comment c, string isbn)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    string sql = "INSERT INTO comments (isbn, reader_id, comment_text, date) VALUES(@isbn, @reader_id, @comment_text, @date)";
                    SqlCommand command = new SqlCommand(sql, connection);

                    connection.Open();

                    command.Parameters.AddWithValue("@isbn", isbn);
                    command.Parameters.AddWithValue("@reader_id", c.Commenter.Id);
                    command.Parameters.AddWithValue("@comment_text", c.CommentText);
                    command.Parameters.AddWithValue("@date", c.Date);

                    command.ExecuteNonQuery();

                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.ToString());
            }
        }

        // METHOD THAT DELETES AN USER

        public void DeleteUser(int id)
        {
            // ToDo: Implement code
        }
    }
}