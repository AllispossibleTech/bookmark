﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public class Comment
    {
        private Reader commenter;
        private string commentText;
        private DateTime date;

        public Comment(Reader commenter, string commentText, DateTime date)
        {
            this.commenter = commenter;
            this.commentText = commentText;
            this.date = date;
        }

        public string CommentText { get { return commentText; } }

        public DateTime Date { get { return date; } }

        public Reader Commenter { get { return commenter; } }

        public string GetInfo()
        {
            return $"{this.date.ToString("dd/MM/yyyy HH:mm")} by {this.commenter.Name}";
        }

    }
}
