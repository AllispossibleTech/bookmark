﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public enum Genre
    {
        Literary_Fiction,
        Mystery,
        Thriller,
        Horror,
        Historical,
        Romance,
        Western,
        Science_Fiction,
        Fantasy,
        Dystopian,
        Magical_Realism,
        Realist_Literature
    }
}
