﻿namespace DomainClassLibrary
{
    public class Author : User
    {
        public List<Book> PublishedBooks { get; private set; }

        public Author(string name, string surname, string email, string password, string country, int id) : base(name, surname, email, password, country, id)
        {
            PublishedBooks = new List<Book>();
        }

        public Author(string name, string surname, string email, string password, string country) : base(name, surname, email, password, country)
        {
            PublishedBooks = new List<Book>();
        }

        public void AddBook(Book b)
        { 
            this.PublishedBooks.Add(b);
        }

        public void RemoveBook(string isbn)
        {
            foreach(Book b in this.PublishedBooks)
            {
                if (b.Isbn == isbn)
                {
                    this.PublishedBooks.Remove(b);
                }
            }
        }

        public List<Book> GetBooks()
        {
            return this.PublishedBooks;
        }

        public Book GetBook(string isbn)
        {
            foreach(Book b in this.PublishedBooks)
            {
                if(b.Isbn.Equals(isbn))
                {
                    return b;
                }
            }
            return null;
        }
    }
}