﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public interface IUserRepository
    {
        List<User> GetAll();

        User GetUser(int id);

        List<Author> GetAuthors();

        Author GetAuthorById(int id);

        List<Book> GetBooksByAuthor(Author a);

        List<Comment> GetCommentsByIsbn(string isbn);

        List<Reader> GetReaders();

        Reader GetReader(int id);

        List<Genre> GetPreferredGenresByReaderId(int id);

        List<Book> GetReadBooksByReaderId(int id);

        void AddBook(Book b);

        void AddVote(int id, int voteNr);

        void AddRating(Book b);

        void AddComment(Comment c, string isbn);

        bool Exists(int id);

        void Add(User user);

        void DeleteUser(int id);
    }
}
