﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public class Reader : User
    {

        public Reader(string name, string surname, string email, string password, string country, List<Genre> lovedGenres, int preferredNumberOfPages) : base (name, surname, email, password, country)
        {
            this.NrOfVotes = 0;
            this.LovedGenres = lovedGenres;
            this.PreferredNumberOfPages = preferredNumberOfPages;
            this.ReadBooks = new List<Book>();

        }

        public Reader(string name, string surname, string email, string password, string country, int id, int preferredNumberOfPages, int nrOfVotes) : base(name, surname, email, password, country, id)
        {
            this.NrOfVotes = nrOfVotes;
            this.PreferredNumberOfPages = preferredNumberOfPages;
            this.ReadBooks = new List<Book>();
            this.LovedGenres = new List<Genre>();

        }

        // Change setters to private (UserRepository way of working needs them public at them moment, change the way Readers are read from DB)

        public List<Genre> LovedGenres { get; set; }

        public List<Book> ReadBooks { get; set; }

        public int PreferredNumberOfPages { get; set; }

        public int NrOfVotes { get; set; }

        public void AddVote()
        {
            this.NrOfVotes += 1;
        }
    }
}
