﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public class BookMark
    {
        IUserRepository userRepository;

        public BookMark(IUserRepository userRepository)
        {
            this.userRepository = userRepository;
        }

        public List<User> GetUsers()
        {
            return this.userRepository.GetAll();
            // ToDo: change database design from table-per-type to single table with nullable values, limited differences in the two sub classes, more convenient. Moreover, currently the Id is not assigned/checked by the Domain Layer Logic but is done directly in the database using "Sequences" and that is not good, not unit testable.
        }

        public User GetUser(int id)
        {
            foreach(User u in this.GetUsers())
            {
                if (u.Id == id)
                {
                    return u;
                }
            }
            return null;
            // This method can also be thought of as Exist() : if the user is not found, null will be returned, if there is no ID matching it, there isn't the requested user.
        }

        public int GetUserId(string username, string password)
        {
            // HASHING
            // PasswordManager passwordManager = new PasswordManager();
            // string hashed = passwordManager.HashPassword(password);
            
            foreach (User u in this.GetUsers())
            {
                if (u.Email == username && u.Password == password)
                {
                    return u.Id;
                }
            }
            return -1;
            // ToDo: Implement Hashing Here, change DB to accept longer values
        }

        public bool AddUser(User user)
        {
            if (user != null && this.GetUser(user.Id) == null)
            {
                this.userRepository.Add(user);
                return true;
            }
            return false;
        }

        public List<Book> GetBooks()
        {
            List<Book> books = new List<Book>();
            foreach(User a in this.GetUsers())
            {
                if (a is Author)
                {
                    foreach(Book b in ((Author)a).GetBooks())
                    {
                        books.Add(b);
                    }
                }
            }
            return books;
        }

        public List<Book> GetPopularBooks()
        {
            List<Book> result = new List<Book>();

            foreach (Book b in this.GetBooks())
            {
                if (b.GetRating() > 4)
                {
                    result.Add(b);
                }
            }
            return result;
        }

        public Book GetBook(string isbn)
        {
            foreach(Book b in this.GetBooks())
            {
                if (b.Isbn == isbn)
                {
                    return b;
                }
            }
            return null;
            // This method also checks if a book actually exists based on the ISBN
        }

        public bool AddBook(Book b)
        {
            if (this.GetBook(b.Isbn) == null && b.Isbn.Length < 14 && Regex.IsMatch(b.Isbn, "^[0-9]+$"))
            {   
                this.userRepository.AddBook(b);
                return true;
                // In order for a book to be added, there shouldn't be one with the same ISBN, it cannot be more than 13 characters long and it should contain only numbers.
            }
            return false;
        }

        public void AddRating(int vote, string isbn, int readerId)
        {
             if (this.GetUser(readerId) != null && this.GetBook(isbn) != null)
             {
                this.userRepository.AddVote(readerId, 1+((Reader)GetUser(readerId)).NrOfVotes);
                Book b = this.GetBook(isbn);
                b.AddRating(vote);
                this.userRepository.AddRating(b);

                // ToDo: normalize the Books table in the database. Currently the rating of a book is a number saved on the table but I have to add a table just for "Ratings" so that the book is not updated each time but a new rating is added. Otherwise, if multiple users try to add a rating for the same book at the same time, there could be a problem.
             }
        }

        public void AddComment(Comment c, Book b)
        {
            this.userRepository.AddComment(c, b.Isbn);
        }

        public List<Book> GetSuggestedBooks(Reader reader)
        {
            List<Book> result = new List<Book>();
            foreach (Book b in this.GetBooks())
            {
                if (reader.LovedGenres.Contains(b.Genre) && !reader.ReadBooks.Contains(b) && b.PageNumber >= reader.PreferredNumberOfPages -50 && b.PageNumber <= reader.PreferredNumberOfPages + 50)
                {
                    result.Add(b);
                }
            }
            return result;
            // ToDo: Add possibility for a reader to select a range of book number of pages. Hardcoding a range of 100 pages around the specified "PreferredNumberOfPages" is not great.
        }
    }
}
