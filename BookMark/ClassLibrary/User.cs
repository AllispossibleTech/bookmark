﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public abstract class User
    {
        public string Name { get; private set; }

        public string Surname { get; private set; }

        public string FullName { get; private set; }

        public string Email { get; private set; }

        public string Password { get; private set; }

        public string Country { get; private set; }

        public int Id { get; set; }

        public User(string name, string surname, string email, string password, string country, int id)
        {
            this.Name = name;
            this.Surname = surname;
            this.FullName = $"{name} {surname}";
            this.Email = email;
            this.Password = password;
            this.Country = country;
            this.Id = id;
        }

        public User(string name, string surname, string email, string password, string country)
        {
            this.Name = name;
            this.Surname = surname;
            this.FullName = $"{name} {surname}";
            this.Email = email;
            this.Password = password;
            this.Country = country;
        }

    }
}
