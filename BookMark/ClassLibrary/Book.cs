﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DomainClassLibrary
{
    public class Book
    {
        private string isbn;
        private string title;
        private string description;
        private DateTime publishDate;
        private Genre genre;
        private int pageNumber;
        private int totalRatings;
        private int nrOfRatings;
        private List<Comment> comments;

        public Book()
        {
        }

        public Book(string isbn, string title, Author author, string description, DateTime publishDate, Genre genre, int pageNumber)
        {
            this.isbn = isbn;
            this.title = title;
            this.Author = author;
            this.description = description;
            this.publishDate = publishDate;
            this.genre = genre;
            this.pageNumber = pageNumber;
            this.totalRatings = 0;
            this.nrOfRatings = 0;
            this.comments = new List<Comment>();
        }

        public Book(string isbn, string title, Author author, string description, DateTime publishDate, Genre genre, int pageNumber, int totalRatings, int nrOfRatings)
        {
            this.isbn = isbn;
            this.title = title;
            this.Author = author;
            this.description = description;
            this.publishDate = publishDate;
            this.genre = genre;
            this.pageNumber = pageNumber;
            this.totalRatings = totalRatings;
            this.nrOfRatings = nrOfRatings;
            this.comments = new List<Comment>();
        }

        public int NrOfRatings { get { return this.nrOfRatings; } }

        public int TotalRatings { get { return this.totalRatings; } }

        public string Isbn { get { return this.isbn; } }

        // Make set private
        public Author Author { get; set; }

        public string Title { get { return this.title; } }

        public string Description { get { return this.description; } }

        // make set private
        public int PageNumber { get { return this.pageNumber; } set { this.pageNumber = value; } }

        public Genre Genre { get { return this.genre; } }

        public List<Comment> Comments { get { return this.comments; } }

        public DateTime PublishDate { get { return this.publishDate; } }

        public void AddRating(int rating)
        {
            this.nrOfRatings += 1;
            this.totalRatings += rating;
        }

        public double GetRating()
        {
            if (nrOfRatings != 0)
            {
                return totalRatings / nrOfRatings;
            }
            return 0;
        }

        public void AddComment(Comment comment)
        {
            this.comments.Add(comment);
        }

        public DateTime GetPublishDate()
        {
            return this.publishDate;
        }

        public string ToString()
        {
            return $"{this.title} - {this.isbn}";
        }
    }
}
