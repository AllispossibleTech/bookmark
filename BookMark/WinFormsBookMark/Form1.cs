using DataAccessClassLibrary;
using DomainClassLibrary;

namespace WinFormsBookMark
{
    public partial class MainForm : Form
    {
        private BookMark bookMark;
        private Book selectedBook;
        private Reader selectedReader;
        public MainForm()
        {
            InitializeComponent();
            bookMark = new BookMark(new UserRepository());
            UpdateStats();

            // DEBUG STUFF START ---------------------------------------------------

            //List<Genre> genreList = new List<Genre>();
            //genreList.Add(Genre.Mystery);
            //bookMark.AddUser(new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "33jifowd", "IT", 0001));
            //bookMark.AddUser(new Reader("Paolo", "Rossi", "ale@ghesboro.com", "lksdfljsdk", "IT", 0002, genreList, 500));
            //foreach (User u in bookMark.GetUsers())
            //{
            //    if (u is Author)
            //    {
            //        if (u.Id == 0001)
            //        {
            //            ((Author)u).AddBook(new Book("234234-234-2-34234-24", "Nice Title", ((Author)u), "A nice desc.", DateTime.Now, Genre.Mystery, 2));
            //            ((Author)u).AddBook(new Book("234453-435-3-53535-35", "Second Title", ((Author)u), "A much nicer desc.", DateTime.Now.AddDays(-8), Genre.Dystopian, 2));
            //            ((Author)u).GetBook("234234-234-2-34234-24").AddComment(new Comment((Reader)(bookMark.GetUser(0002)), "A very nice book indeed"));
            //        }
            //    }
            //}

            // DEBUG STUFF END -----------------------------------------------------

        }

        private void UpdateStats()
        {
            int booksInTheLastWeek = 0;
            int authors = 0;
            int readers = 0;

            foreach (User u in bookMark.GetUsers())
            {
                if (u is Author)
                {
                    authors += 1;
                } else if (u is Reader)
                {
                    readers += 1;
                }
            }

            foreach (Book b in bookMark.GetBooks())
            {
                if (DateTime.Compare(b.GetPublishDate(), DateTime.Now.AddDays(-7)) >= 0)
                {
                    booksInTheLastWeek += 1;
                }
            }

            lbl_users.Text = $"Total number of Users registered on the system: {this.bookMark.GetUsers().Count}";
            lbl_auth_read.Text = $"of which, {authors} Authors and {readers} Readers.";
            lbl_books.Text = $"Total number of Books registered on the system: {bookMark.GetBooks().Count}";
            lbl_booksweek.Text = $"of which {booksInTheLastWeek} registered in the last week.";
        }

        private void btn_refreshStats_Click(object sender, EventArgs e)
        {
            UpdateStats();
        }

        private void btn_viewBooks_Click(object sender, EventArgs e)
        {
            lbx_books.Items.Clear();

            foreach (Book b in bookMark.GetBooks())
            {
                lbx_books.Items.Add(b.ToString());
            }
        }

        private void btn_bookDetails_Click(object sender, EventArgs e)
        {
            foreach (Book b in bookMark.GetBooks())
            {
                if(b.ToString() == lbx_books.SelectedItem.ToString())
                {
                    this.selectedBook = b;
                    txt_bookTitle.Text = b.Title;
                    txt_bookIsbn.Text = b.Isbn;
                    rtxt_bookDescription.Text = b.Description;
                    lbl_bookDate.Text = "published " + b.GetPublishDate().ToString("dd/MM/yyyy");
                    lbl_bookGenre.Text = "Genre: " + b.Genre.ToString();
                    lbl_bookRating.Text = "Rating: " + b.GetRating();
                    lbl_bookPages.Text = "Page Nr.: " + b.PageNumber;
                    lbx_bookComments.Items.Clear();

                    if (b.Comments.Count != 0)
                    {
                        foreach (Comment c in b.Comments)
                        {
                            lbx_bookComments.Items.Add(c.GetInfo());
                        }
                    } else
                    {
                        lbx_bookComments.Items.Add("No Comments Yet");
                    }
                    
                    break;
                    // FINIREEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE
                }
            }

        }

        private void btn_viewComment_Click(object sender, EventArgs e)
        {
            if (selectedBook != null)
            {
                foreach (Comment c in this.selectedBook.Comments)
                {
                    if(lbx_bookComments.SelectedItem.ToString() == c.GetInfo())
                    {
                        MessageBox.Show($"Comment by {c.Commenter.Name} (ID: {c.Commenter.Id}) - {c.Date.ToString("dd/MM/yyyy HH:mm")}: {c.CommentText}");
                    }
                }
            }
        }

        private void btn_viewReaders_Click(object sender, EventArgs e)
        {
            lbx_readers.Items.Clear();
            foreach (User u in bookMark.GetUsers())
            {
                if (u is Reader)
                {
                    lbx_readers.Items.Add($"{u.Name} {u.Surname}");
                }
            }
        }

        private void btn_readerDetails_Click(object sender, EventArgs e)
        {
            foreach (User u in bookMark.GetUsers())
            {
                if (u.FullName == lbx_readers.SelectedItem.ToString())
                {
                    this.selectedReader = (Reader)u;
                    txt_readerName.Text = u.FullName;
                    txt_readerId.Text = u.Id.ToString();
                    txt_readerEmail.Text = u.Email;
                    txt_readerPreferredNumberOfPages.Text = ((Reader)u).PreferredNumberOfPages.ToString();
                    txt_readerCountry.Text = u.Country;

                    lbx_readerPreferredGenres.Items.Clear();
                    if (((Reader)u).LovedGenres.Count != 0)
                    {
                        foreach (Genre g in ((Reader)u).LovedGenres)
                        {
                            lbx_readerPreferredGenres.Items.Add(g);
                        }
                    }
                    else
                    {
                        lbx_readerPreferredGenres.Items.Add("No Preferred Genres Yet");
                    }
                    break;
                }
            }
        }

        private void btn_viewAuthors_Click(object sender, EventArgs e)
        {
            lbx_authors.Items.Clear();
            foreach (User u in bookMark.GetUsers())
            {
                if (u is Author)
                {
                    lbx_authors.Items.Add($"{u.Name} {u.Surname}");
                }
            }
        }

        private void btn_viewAuthorDetails_Click(object sender, EventArgs e)
        {
            foreach (User u in bookMark.GetUsers())
            {
                if (u.FullName == lbx_authors.SelectedItem.ToString())
                {
                    txt_authorName.Text = u.FullName;
                    txt_authorId.Text = u.Id.ToString();
                    txt_authorEmail.Text = u.Email;
                    txt_authorNumberOfBooks.Text = ((Author)u).PublishedBooks.Count.ToString();
                    txt_authorCountry.Text = u.Country;

                    lbx_authorBooks.Items.Clear();
                    if (((Author)u).PublishedBooks.Count != 0)
                    {
                        foreach (Book b in ((Author)u).PublishedBooks)
                        {
                            lbx_authorBooks.Items.Add(b.Title);
                        }
                    }
                    else
                    {
                        lbx_readerPreferredGenres.Items.Add("No Books Published Yet");
                    }
                    break;
                }
            }
        }
    }
}