﻿namespace WinFormsBookMark
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.admin_tabControl = new System.Windows.Forms.TabControl();
            this.tab_dashboard = new System.Windows.Forms.TabPage();
            this.btn_refreshStats = new System.Windows.Forms.Button();
            this.lbl_booksweek = new System.Windows.Forms.Label();
            this.lbl_books = new System.Windows.Forms.Label();
            this.lbl_auth_read = new System.Windows.Forms.Label();
            this.lbl_users = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tab_books = new System.Windows.Forms.TabPage();
            this.btn_viewComment = new System.Windows.Forms.Button();
            this.lbl_bookComments = new System.Windows.Forms.Label();
            this.lbx_bookComments = new System.Windows.Forms.ListBox();
            this.lbl_bookPages = new System.Windows.Forms.Label();
            this.lbl_bookGenre = new System.Windows.Forms.Label();
            this.lbl_bookRating = new System.Windows.Forms.Label();
            this.rtxt_bookDescription = new System.Windows.Forms.RichTextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbl_bookDate = new System.Windows.Forms.Label();
            this.txt_bookIsbn = new System.Windows.Forms.TextBox();
            this.lbl_bookIsbn = new System.Windows.Forms.Label();
            this.lbl_bookTitle = new System.Windows.Forms.Label();
            this.txt_bookTitle = new System.Windows.Forms.TextBox();
            this.btn_bookDetails = new System.Windows.Forms.Button();
            this.btn_viewBooks = new System.Windows.Forms.Button();
            this.lbx_books = new System.Windows.Forms.ListBox();
            this.tab_readers = new System.Windows.Forms.TabPage();
            this.lbx_readerPreferredGenres = new System.Windows.Forms.ListBox();
            this.lbl_readerPreferredGenres = new System.Windows.Forms.Label();
            this.txt_readerId = new System.Windows.Forms.TextBox();
            this.lbl_readerId = new System.Windows.Forms.Label();
            this.txt_readerPreferredNumberOfPages = new System.Windows.Forms.TextBox();
            this.lbl_readerPreferredNumberOfPages = new System.Windows.Forms.Label();
            this.txt_readerEmail = new System.Windows.Forms.TextBox();
            this.lbl_readerEmail = new System.Windows.Forms.Label();
            this.txt_readerCountry = new System.Windows.Forms.TextBox();
            this.lbl_readerCountry = new System.Windows.Forms.Label();
            this.lbl_readerName = new System.Windows.Forms.Label();
            this.txt_readerName = new System.Windows.Forms.TextBox();
            this.btn_readerDetails = new System.Windows.Forms.Button();
            this.btn_viewReaders = new System.Windows.Forms.Button();
            this.lbx_readers = new System.Windows.Forms.ListBox();
            this.tab_authors = new System.Windows.Forms.TabPage();
            this.lbx_authorBooks = new System.Windows.Forms.ListBox();
            this.lbl_authorBooks = new System.Windows.Forms.Label();
            this.txt_authorId = new System.Windows.Forms.TextBox();
            this.lbl_authorId = new System.Windows.Forms.Label();
            this.txt_authorNumberOfBooks = new System.Windows.Forms.TextBox();
            this.lbl_authorNumberOfBooks = new System.Windows.Forms.Label();
            this.txt_authorEmail = new System.Windows.Forms.TextBox();
            this.lbl_authorEmail = new System.Windows.Forms.Label();
            this.txt_authorCountry = new System.Windows.Forms.TextBox();
            this.lbl_authorCountry = new System.Windows.Forms.Label();
            this.lbl_authorName = new System.Windows.Forms.Label();
            this.txt_authorName = new System.Windows.Forms.TextBox();
            this.btn_viewAuthorDetails = new System.Windows.Forms.Button();
            this.btn_viewAuthors = new System.Windows.Forms.Button();
            this.lbx_authors = new System.Windows.Forms.ListBox();
            this.admin_tabControl.SuspendLayout();
            this.tab_dashboard.SuspendLayout();
            this.tab_books.SuspendLayout();
            this.tab_readers.SuspendLayout();
            this.tab_authors.SuspendLayout();
            this.SuspendLayout();
            // 
            // admin_tabControl
            // 
            this.admin_tabControl.Controls.Add(this.tab_dashboard);
            this.admin_tabControl.Controls.Add(this.tab_books);
            this.admin_tabControl.Controls.Add(this.tab_readers);
            this.admin_tabControl.Controls.Add(this.tab_authors);
            this.admin_tabControl.Location = new System.Drawing.Point(38, 50);
            this.admin_tabControl.Name = "admin_tabControl";
            this.admin_tabControl.SelectedIndex = 0;
            this.admin_tabControl.Size = new System.Drawing.Size(1395, 778);
            this.admin_tabControl.TabIndex = 0;
            // 
            // tab_dashboard
            // 
            this.tab_dashboard.Controls.Add(this.btn_refreshStats);
            this.tab_dashboard.Controls.Add(this.lbl_booksweek);
            this.tab_dashboard.Controls.Add(this.lbl_books);
            this.tab_dashboard.Controls.Add(this.lbl_auth_read);
            this.tab_dashboard.Controls.Add(this.lbl_users);
            this.tab_dashboard.Controls.Add(this.label2);
            this.tab_dashboard.Controls.Add(this.label1);
            this.tab_dashboard.Location = new System.Drawing.Point(8, 46);
            this.tab_dashboard.Name = "tab_dashboard";
            this.tab_dashboard.Padding = new System.Windows.Forms.Padding(3);
            this.tab_dashboard.Size = new System.Drawing.Size(1379, 724);
            this.tab_dashboard.TabIndex = 0;
            this.tab_dashboard.Text = "Dashboard";
            this.tab_dashboard.UseVisualStyleBackColor = true;
            // 
            // btn_refreshStats
            // 
            this.btn_refreshStats.Location = new System.Drawing.Point(532, 582);
            this.btn_refreshStats.Name = "btn_refreshStats";
            this.btn_refreshStats.Size = new System.Drawing.Size(279, 65);
            this.btn_refreshStats.TabIndex = 9;
            this.btn_refreshStats.Text = "Refresh";
            this.btn_refreshStats.UseVisualStyleBackColor = true;
            this.btn_refreshStats.Click += new System.EventHandler(this.btn_refreshStats_Click);
            // 
            // lbl_booksweek
            // 
            this.lbl_booksweek.AutoSize = true;
            this.lbl_booksweek.Location = new System.Drawing.Point(59, 448);
            this.lbl_booksweek.Name = "lbl_booksweek";
            this.lbl_booksweek.Size = new System.Drawing.Size(447, 32);
            this.lbl_booksweek.TabIndex = 5;
            this.lbl_booksweek.Text = "of which N/A registered in the last week.";
            // 
            // lbl_books
            // 
            this.lbl_books.AutoSize = true;
            this.lbl_books.Location = new System.Drawing.Point(59, 393);
            this.lbl_books.Name = "lbl_books";
            this.lbl_books.Size = new System.Drawing.Size(582, 32);
            this.lbl_books.TabIndex = 4;
            this.lbl_books.Text = "Total number of Books registered on the system: N/A";
            // 
            // lbl_auth_read
            // 
            this.lbl_auth_read.AutoSize = true;
            this.lbl_auth_read.Location = new System.Drawing.Point(59, 302);
            this.lbl_auth_read.Name = "lbl_auth_read";
            this.lbl_auth_read.Size = new System.Drawing.Size(440, 32);
            this.lbl_auth_read.TabIndex = 3;
            this.lbl_auth_read.Text = "of which, N/A Authors and N/A Readers.";
            // 
            // lbl_users
            // 
            this.lbl_users.AutoSize = true;
            this.lbl_users.Location = new System.Drawing.Point(59, 251);
            this.lbl_users.Name = "lbl_users";
            this.lbl_users.Size = new System.Drawing.Size(575, 32);
            this.lbl_users.TabIndex = 2;
            this.lbl_users.Text = "Total number of Users registered on the system: N/A";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(547, 102);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(273, 32);
            this.label2.TabIndex = 1;
            this.label2.Text = "--- BookMark Admin ---";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 16.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(523, 43);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(319, 59);
            this.label1.TabIndex = 0;
            this.label1.Text = "Welcome Back";
            // 
            // tab_books
            // 
            this.tab_books.Controls.Add(this.btn_viewComment);
            this.tab_books.Controls.Add(this.lbl_bookComments);
            this.tab_books.Controls.Add(this.lbx_bookComments);
            this.tab_books.Controls.Add(this.lbl_bookPages);
            this.tab_books.Controls.Add(this.lbl_bookGenre);
            this.tab_books.Controls.Add(this.lbl_bookRating);
            this.tab_books.Controls.Add(this.rtxt_bookDescription);
            this.tab_books.Controls.Add(this.label6);
            this.tab_books.Controls.Add(this.lbl_bookDate);
            this.tab_books.Controls.Add(this.txt_bookIsbn);
            this.tab_books.Controls.Add(this.lbl_bookIsbn);
            this.tab_books.Controls.Add(this.lbl_bookTitle);
            this.tab_books.Controls.Add(this.txt_bookTitle);
            this.tab_books.Controls.Add(this.btn_bookDetails);
            this.tab_books.Controls.Add(this.btn_viewBooks);
            this.tab_books.Controls.Add(this.lbx_books);
            this.tab_books.Location = new System.Drawing.Point(8, 46);
            this.tab_books.Name = "tab_books";
            this.tab_books.Size = new System.Drawing.Size(1379, 724);
            this.tab_books.TabIndex = 3;
            this.tab_books.Text = "Books";
            this.tab_books.UseVisualStyleBackColor = true;
            // 
            // btn_viewComment
            // 
            this.btn_viewComment.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_viewComment.Location = new System.Drawing.Point(1192, 565);
            this.btn_viewComment.Name = "btn_viewComment";
            this.btn_viewComment.Size = new System.Drawing.Size(102, 132);
            this.btn_viewComment.TabIndex = 15;
            this.btn_viewComment.Text = "View";
            this.btn_viewComment.UseVisualStyleBackColor = true;
            this.btn_viewComment.Click += new System.EventHandler(this.btn_viewComment_Click);
            // 
            // lbl_bookComments
            // 
            this.lbl_bookComments.AutoSize = true;
            this.lbl_bookComments.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookComments.Location = new System.Drawing.Point(620, 565);
            this.lbl_bookComments.Name = "lbl_bookComments";
            this.lbl_bookComments.Size = new System.Drawing.Size(151, 37);
            this.lbl_bookComments.TabIndex = 14;
            this.lbl_bookComments.Text = "Comments:";
            // 
            // lbx_bookComments
            // 
            this.lbx_bookComments.FormattingEnabled = true;
            this.lbx_bookComments.ItemHeight = 32;
            this.lbx_bookComments.Location = new System.Drawing.Point(780, 565);
            this.lbx_bookComments.Name = "lbx_bookComments";
            this.lbx_bookComments.Size = new System.Drawing.Size(406, 132);
            this.lbx_bookComments.TabIndex = 13;
            // 
            // lbl_bookPages
            // 
            this.lbl_bookPages.AutoSize = true;
            this.lbl_bookPages.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookPages.Location = new System.Drawing.Point(1116, 233);
            this.lbl_bookPages.Name = "lbl_bookPages";
            this.lbl_bookPages.Size = new System.Drawing.Size(178, 37);
            this.lbl_bookPages.TabIndex = 12;
            this.lbl_bookPages.Text = "Page Nr.: N/A";
            // 
            // lbl_bookGenre
            // 
            this.lbl_bookGenre.AutoSize = true;
            this.lbl_bookGenre.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookGenre.Location = new System.Drawing.Point(780, 233);
            this.lbl_bookGenre.Name = "lbl_bookGenre";
            this.lbl_bookGenre.Size = new System.Drawing.Size(149, 37);
            this.lbl_bookGenre.TabIndex = 11;
            this.lbl_bookGenre.Text = "Genre: N/A";
            // 
            // lbl_bookRating
            // 
            this.lbl_bookRating.AutoSize = true;
            this.lbl_bookRating.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookRating.Location = new System.Drawing.Point(1146, 174);
            this.lbl_bookRating.Name = "lbl_bookRating";
            this.lbl_bookRating.Size = new System.Drawing.Size(148, 37);
            this.lbl_bookRating.TabIndex = 10;
            this.lbl_bookRating.Text = "rating: N/A";
            // 
            // rtxt_bookDescription
            // 
            this.rtxt_bookDescription.Location = new System.Drawing.Point(780, 286);
            this.rtxt_bookDescription.Name = "rtxt_bookDescription";
            this.rtxt_bookDescription.Size = new System.Drawing.Size(514, 195);
            this.rtxt_bookDescription.TabIndex = 9;
            this.rtxt_bookDescription.Text = "";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(616, 285);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 37);
            this.label6.TabIndex = 8;
            this.label6.Text = "Description:";
            // 
            // lbl_bookDate
            // 
            this.lbl_bookDate.AutoSize = true;
            this.lbl_bookDate.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookDate.Location = new System.Drawing.Point(780, 174);
            this.lbl_bookDate.Name = "lbl_bookDate";
            this.lbl_bookDate.Size = new System.Drawing.Size(189, 37);
            this.lbl_bookDate.TabIndex = 7;
            this.lbl_bookDate.Text = "published N/A";
            // 
            // txt_bookIsbn
            // 
            this.txt_bookIsbn.Location = new System.Drawing.Point(780, 115);
            this.txt_bookIsbn.Name = "txt_bookIsbn";
            this.txt_bookIsbn.Size = new System.Drawing.Size(514, 39);
            this.txt_bookIsbn.TabIndex = 6;
            // 
            // lbl_bookIsbn
            // 
            this.lbl_bookIsbn.AutoSize = true;
            this.lbl_bookIsbn.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookIsbn.Location = new System.Drawing.Point(692, 115);
            this.lbl_bookIsbn.Name = "lbl_bookIsbn";
            this.lbl_bookIsbn.Size = new System.Drawing.Size(79, 37);
            this.lbl_bookIsbn.TabIndex = 5;
            this.lbl_bookIsbn.Text = "ISBN:";
            // 
            // lbl_bookTitle
            // 
            this.lbl_bookTitle.AutoSize = true;
            this.lbl_bookTitle.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_bookTitle.Location = new System.Drawing.Point(692, 61);
            this.lbl_bookTitle.Name = "lbl_bookTitle";
            this.lbl_bookTitle.Size = new System.Drawing.Size(82, 37);
            this.lbl_bookTitle.TabIndex = 4;
            this.lbl_bookTitle.Text = "Title:";
            // 
            // txt_bookTitle
            // 
            this.txt_bookTitle.Location = new System.Drawing.Point(780, 59);
            this.txt_bookTitle.Name = "txt_bookTitle";
            this.txt_bookTitle.Size = new System.Drawing.Size(514, 39);
            this.txt_bookTitle.TabIndex = 3;
            // 
            // btn_bookDetails
            // 
            this.btn_bookDetails.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_bookDetails.Location = new System.Drawing.Point(506, 59);
            this.btn_bookDetails.Name = "btn_bookDetails";
            this.btn_bookDetails.Size = new System.Drawing.Size(89, 580);
            this.btn_bookDetails.TabIndex = 2;
            this.btn_bookDetails.Text = "➡️";
            this.btn_bookDetails.UseVisualStyleBackColor = true;
            this.btn_bookDetails.Click += new System.EventHandler(this.btn_bookDetails_Click);
            // 
            // btn_viewBooks
            // 
            this.btn_viewBooks.Location = new System.Drawing.Point(51, 660);
            this.btn_viewBooks.Name = "btn_viewBooks";
            this.btn_viewBooks.Size = new System.Drawing.Size(438, 46);
            this.btn_viewBooks.TabIndex = 1;
            this.btn_viewBooks.Text = "View Books";
            this.btn_viewBooks.UseVisualStyleBackColor = true;
            this.btn_viewBooks.Click += new System.EventHandler(this.btn_viewBooks_Click);
            // 
            // lbx_books
            // 
            this.lbx_books.FormattingEnabled = true;
            this.lbx_books.ItemHeight = 32;
            this.lbx_books.Location = new System.Drawing.Point(51, 59);
            this.lbx_books.Name = "lbx_books";
            this.lbx_books.Size = new System.Drawing.Size(438, 580);
            this.lbx_books.TabIndex = 0;
            // 
            // tab_readers
            // 
            this.tab_readers.Controls.Add(this.lbx_readerPreferredGenres);
            this.tab_readers.Controls.Add(this.lbl_readerPreferredGenres);
            this.tab_readers.Controls.Add(this.txt_readerId);
            this.tab_readers.Controls.Add(this.lbl_readerId);
            this.tab_readers.Controls.Add(this.txt_readerPreferredNumberOfPages);
            this.tab_readers.Controls.Add(this.lbl_readerPreferredNumberOfPages);
            this.tab_readers.Controls.Add(this.txt_readerEmail);
            this.tab_readers.Controls.Add(this.lbl_readerEmail);
            this.tab_readers.Controls.Add(this.txt_readerCountry);
            this.tab_readers.Controls.Add(this.lbl_readerCountry);
            this.tab_readers.Controls.Add(this.lbl_readerName);
            this.tab_readers.Controls.Add(this.txt_readerName);
            this.tab_readers.Controls.Add(this.btn_readerDetails);
            this.tab_readers.Controls.Add(this.btn_viewReaders);
            this.tab_readers.Controls.Add(this.lbx_readers);
            this.tab_readers.Location = new System.Drawing.Point(8, 46);
            this.tab_readers.Name = "tab_readers";
            this.tab_readers.Padding = new System.Windows.Forms.Padding(3);
            this.tab_readers.Size = new System.Drawing.Size(1379, 724);
            this.tab_readers.TabIndex = 1;
            this.tab_readers.Text = "Manage Readers";
            this.tab_readers.UseVisualStyleBackColor = true;
            // 
            // lbx_readerPreferredGenres
            // 
            this.lbx_readerPreferredGenres.FormattingEnabled = true;
            this.lbx_readerPreferredGenres.ItemHeight = 32;
            this.lbx_readerPreferredGenres.Location = new System.Drawing.Point(648, 342);
            this.lbx_readerPreferredGenres.Name = "lbx_readerPreferredGenres";
            this.lbx_readerPreferredGenres.Size = new System.Drawing.Size(619, 132);
            this.lbx_readerPreferredGenres.TabIndex = 18;
            // 
            // lbl_readerPreferredGenres
            // 
            this.lbl_readerPreferredGenres.AutoSize = true;
            this.lbl_readerPreferredGenres.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerPreferredGenres.Location = new System.Drawing.Point(648, 289);
            this.lbl_readerPreferredGenres.Name = "lbl_readerPreferredGenres";
            this.lbl_readerPreferredGenres.Size = new System.Drawing.Size(220, 37);
            this.lbl_readerPreferredGenres.TabIndex = 17;
            this.lbl_readerPreferredGenres.Text = "Preferred Genres:";
            // 
            // txt_readerId
            // 
            this.txt_readerId.Location = new System.Drawing.Point(771, 114);
            this.txt_readerId.Name = "txt_readerId";
            this.txt_readerId.Size = new System.Drawing.Size(177, 39);
            this.txt_readerId.TabIndex = 16;
            // 
            // lbl_readerId
            // 
            this.lbl_readerId.AutoSize = true;
            this.lbl_readerId.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerId.Location = new System.Drawing.Point(716, 115);
            this.lbl_readerId.Name = "lbl_readerId";
            this.lbl_readerId.Size = new System.Drawing.Size(49, 37);
            this.lbl_readerId.TabIndex = 15;
            this.lbl_readerId.Text = "ID:";
            // 
            // txt_readerPreferredNumberOfPages
            // 
            this.txt_readerPreferredNumberOfPages.Location = new System.Drawing.Point(1005, 224);
            this.txt_readerPreferredNumberOfPages.Name = "txt_readerPreferredNumberOfPages";
            this.txt_readerPreferredNumberOfPages.Size = new System.Drawing.Size(262, 39);
            this.txt_readerPreferredNumberOfPages.TabIndex = 14;
            // 
            // lbl_readerPreferredNumberOfPages
            // 
            this.lbl_readerPreferredNumberOfPages.AutoSize = true;
            this.lbl_readerPreferredNumberOfPages.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerPreferredNumberOfPages.Location = new System.Drawing.Point(648, 223);
            this.lbl_readerPreferredNumberOfPages.Name = "lbl_readerPreferredNumberOfPages";
            this.lbl_readerPreferredNumberOfPages.Size = new System.Drawing.Size(351, 37);
            this.lbl_readerPreferredNumberOfPages.TabIndex = 13;
            this.lbl_readerPreferredNumberOfPages.Text = "Preferred Number OF Pages:";
            // 
            // txt_readerEmail
            // 
            this.txt_readerEmail.Location = new System.Drawing.Point(771, 165);
            this.txt_readerEmail.Name = "txt_readerEmail";
            this.txt_readerEmail.Size = new System.Drawing.Size(496, 39);
            this.txt_readerEmail.TabIndex = 12;
            // 
            // lbl_readerEmail
            // 
            this.lbl_readerEmail.AutoSize = true;
            this.lbl_readerEmail.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerEmail.Location = new System.Drawing.Point(677, 165);
            this.lbl_readerEmail.Name = "lbl_readerEmail";
            this.lbl_readerEmail.Size = new System.Drawing.Size(88, 37);
            this.lbl_readerEmail.TabIndex = 11;
            this.lbl_readerEmail.Text = "Email:";
            // 
            // txt_readerCountry
            // 
            this.txt_readerCountry.Location = new System.Drawing.Point(1111, 113);
            this.txt_readerCountry.Name = "txt_readerCountry";
            this.txt_readerCountry.Size = new System.Drawing.Size(156, 39);
            this.txt_readerCountry.TabIndex = 10;
            // 
            // lbl_readerCountry
            // 
            this.lbl_readerCountry.AutoSize = true;
            this.lbl_readerCountry.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerCountry.Location = new System.Drawing.Point(988, 115);
            this.lbl_readerCountry.Name = "lbl_readerCountry";
            this.lbl_readerCountry.Size = new System.Drawing.Size(117, 37);
            this.lbl_readerCountry.TabIndex = 9;
            this.lbl_readerCountry.Text = "Country:";
            // 
            // lbl_readerName
            // 
            this.lbl_readerName.AutoSize = true;
            this.lbl_readerName.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_readerName.Location = new System.Drawing.Point(665, 61);
            this.lbl_readerName.Name = "lbl_readerName";
            this.lbl_readerName.Size = new System.Drawing.Size(100, 37);
            this.lbl_readerName.TabIndex = 8;
            this.lbl_readerName.Text = "Name:";
            // 
            // txt_readerName
            // 
            this.txt_readerName.Location = new System.Drawing.Point(763, 59);
            this.txt_readerName.Name = "txt_readerName";
            this.txt_readerName.Size = new System.Drawing.Size(514, 39);
            this.txt_readerName.TabIndex = 7;
            // 
            // btn_readerDetails
            // 
            this.btn_readerDetails.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_readerDetails.Location = new System.Drawing.Point(513, 59);
            this.btn_readerDetails.Name = "btn_readerDetails";
            this.btn_readerDetails.Size = new System.Drawing.Size(89, 580);
            this.btn_readerDetails.TabIndex = 3;
            this.btn_readerDetails.Text = "➡️";
            this.btn_readerDetails.UseVisualStyleBackColor = true;
            this.btn_readerDetails.Click += new System.EventHandler(this.btn_readerDetails_Click);
            // 
            // btn_viewReaders
            // 
            this.btn_viewReaders.Location = new System.Drawing.Point(51, 660);
            this.btn_viewReaders.Name = "btn_viewReaders";
            this.btn_viewReaders.Size = new System.Drawing.Size(438, 46);
            this.btn_viewReaders.TabIndex = 2;
            this.btn_viewReaders.Text = "View Readers";
            this.btn_viewReaders.UseVisualStyleBackColor = true;
            this.btn_viewReaders.Click += new System.EventHandler(this.btn_viewReaders_Click);
            // 
            // lbx_readers
            // 
            this.lbx_readers.FormattingEnabled = true;
            this.lbx_readers.ItemHeight = 32;
            this.lbx_readers.Location = new System.Drawing.Point(51, 59);
            this.lbx_readers.Name = "lbx_readers";
            this.lbx_readers.Size = new System.Drawing.Size(438, 580);
            this.lbx_readers.TabIndex = 1;
            // 
            // tab_authors
            // 
            this.tab_authors.Controls.Add(this.lbx_authorBooks);
            this.tab_authors.Controls.Add(this.lbl_authorBooks);
            this.tab_authors.Controls.Add(this.txt_authorId);
            this.tab_authors.Controls.Add(this.lbl_authorId);
            this.tab_authors.Controls.Add(this.txt_authorNumberOfBooks);
            this.tab_authors.Controls.Add(this.lbl_authorNumberOfBooks);
            this.tab_authors.Controls.Add(this.txt_authorEmail);
            this.tab_authors.Controls.Add(this.lbl_authorEmail);
            this.tab_authors.Controls.Add(this.txt_authorCountry);
            this.tab_authors.Controls.Add(this.lbl_authorCountry);
            this.tab_authors.Controls.Add(this.lbl_authorName);
            this.tab_authors.Controls.Add(this.txt_authorName);
            this.tab_authors.Controls.Add(this.btn_viewAuthorDetails);
            this.tab_authors.Controls.Add(this.btn_viewAuthors);
            this.tab_authors.Controls.Add(this.lbx_authors);
            this.tab_authors.Location = new System.Drawing.Point(8, 46);
            this.tab_authors.Name = "tab_authors";
            this.tab_authors.Size = new System.Drawing.Size(1379, 724);
            this.tab_authors.TabIndex = 2;
            this.tab_authors.Text = "Manage Authors";
            this.tab_authors.UseVisualStyleBackColor = true;
            // 
            // lbx_authorBooks
            // 
            this.lbx_authorBooks.FormattingEnabled = true;
            this.lbx_authorBooks.ItemHeight = 32;
            this.lbx_authorBooks.Location = new System.Drawing.Point(678, 322);
            this.lbx_authorBooks.Name = "lbx_authorBooks";
            this.lbx_authorBooks.Size = new System.Drawing.Size(619, 132);
            this.lbx_authorBooks.TabIndex = 33;
            // 
            // lbl_authorBooks
            // 
            this.lbl_authorBooks.AutoSize = true;
            this.lbl_authorBooks.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorBooks.Location = new System.Drawing.Point(678, 269);
            this.lbl_authorBooks.Name = "lbl_authorBooks";
            this.lbl_authorBooks.Size = new System.Drawing.Size(217, 37);
            this.lbl_authorBooks.TabIndex = 32;
            this.lbl_authorBooks.Text = "Published Books:";
            // 
            // txt_authorId
            // 
            this.txt_authorId.Location = new System.Drawing.Point(801, 94);
            this.txt_authorId.Name = "txt_authorId";
            this.txt_authorId.Size = new System.Drawing.Size(177, 39);
            this.txt_authorId.TabIndex = 31;
            // 
            // lbl_authorId
            // 
            this.lbl_authorId.AutoSize = true;
            this.lbl_authorId.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorId.Location = new System.Drawing.Point(746, 95);
            this.lbl_authorId.Name = "lbl_authorId";
            this.lbl_authorId.Size = new System.Drawing.Size(49, 37);
            this.lbl_authorId.TabIndex = 30;
            this.lbl_authorId.Text = "ID:";
            // 
            // txt_authorNumberOfBooks
            // 
            this.txt_authorNumberOfBooks.Location = new System.Drawing.Point(1035, 204);
            this.txt_authorNumberOfBooks.Name = "txt_authorNumberOfBooks";
            this.txt_authorNumberOfBooks.Size = new System.Drawing.Size(262, 39);
            this.txt_authorNumberOfBooks.TabIndex = 29;
            // 
            // lbl_authorNumberOfBooks
            // 
            this.lbl_authorNumberOfBooks.AutoSize = true;
            this.lbl_authorNumberOfBooks.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorNumberOfBooks.Location = new System.Drawing.Point(678, 203);
            this.lbl_authorNumberOfBooks.Name = "lbl_authorNumberOfBooks";
            this.lbl_authorNumberOfBooks.Size = new System.Drawing.Size(362, 37);
            this.lbl_authorNumberOfBooks.TabIndex = 28;
            this.lbl_authorNumberOfBooks.Text = "Number OF Published books:";
            // 
            // txt_authorEmail
            // 
            this.txt_authorEmail.Location = new System.Drawing.Point(801, 145);
            this.txt_authorEmail.Name = "txt_authorEmail";
            this.txt_authorEmail.Size = new System.Drawing.Size(496, 39);
            this.txt_authorEmail.TabIndex = 27;
            // 
            // lbl_authorEmail
            // 
            this.lbl_authorEmail.AutoSize = true;
            this.lbl_authorEmail.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorEmail.Location = new System.Drawing.Point(707, 145);
            this.lbl_authorEmail.Name = "lbl_authorEmail";
            this.lbl_authorEmail.Size = new System.Drawing.Size(88, 37);
            this.lbl_authorEmail.TabIndex = 26;
            this.lbl_authorEmail.Text = "Email:";
            // 
            // txt_authorCountry
            // 
            this.txt_authorCountry.Location = new System.Drawing.Point(1141, 93);
            this.txt_authorCountry.Name = "txt_authorCountry";
            this.txt_authorCountry.Size = new System.Drawing.Size(156, 39);
            this.txt_authorCountry.TabIndex = 25;
            // 
            // lbl_authorCountry
            // 
            this.lbl_authorCountry.AutoSize = true;
            this.lbl_authorCountry.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorCountry.Location = new System.Drawing.Point(1018, 95);
            this.lbl_authorCountry.Name = "lbl_authorCountry";
            this.lbl_authorCountry.Size = new System.Drawing.Size(117, 37);
            this.lbl_authorCountry.TabIndex = 24;
            this.lbl_authorCountry.Text = "Country:";
            // 
            // lbl_authorName
            // 
            this.lbl_authorName.AutoSize = true;
            this.lbl_authorName.Font = new System.Drawing.Font("Segoe UI", 10.125F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbl_authorName.Location = new System.Drawing.Point(695, 41);
            this.lbl_authorName.Name = "lbl_authorName";
            this.lbl_authorName.Size = new System.Drawing.Size(100, 37);
            this.lbl_authorName.TabIndex = 23;
            this.lbl_authorName.Text = "Name:";
            // 
            // txt_authorName
            // 
            this.txt_authorName.Location = new System.Drawing.Point(791, 39);
            this.txt_authorName.Name = "txt_authorName";
            this.txt_authorName.Size = new System.Drawing.Size(514, 39);
            this.txt_authorName.TabIndex = 22;
            // 
            // btn_viewAuthorDetails
            // 
            this.btn_viewAuthorDetails.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.btn_viewAuthorDetails.Location = new System.Drawing.Point(543, 39);
            this.btn_viewAuthorDetails.Name = "btn_viewAuthorDetails";
            this.btn_viewAuthorDetails.Size = new System.Drawing.Size(89, 580);
            this.btn_viewAuthorDetails.TabIndex = 21;
            this.btn_viewAuthorDetails.Text = "➡️";
            this.btn_viewAuthorDetails.UseVisualStyleBackColor = true;
            this.btn_viewAuthorDetails.Click += new System.EventHandler(this.btn_viewAuthorDetails_Click);
            // 
            // btn_viewAuthors
            // 
            this.btn_viewAuthors.Location = new System.Drawing.Point(81, 640);
            this.btn_viewAuthors.Name = "btn_viewAuthors";
            this.btn_viewAuthors.Size = new System.Drawing.Size(438, 46);
            this.btn_viewAuthors.TabIndex = 20;
            this.btn_viewAuthors.Text = "View Authors";
            this.btn_viewAuthors.UseVisualStyleBackColor = true;
            this.btn_viewAuthors.Click += new System.EventHandler(this.btn_viewAuthors_Click);
            // 
            // lbx_authors
            // 
            this.lbx_authors.FormattingEnabled = true;
            this.lbx_authors.ItemHeight = 32;
            this.lbx_authors.Location = new System.Drawing.Point(81, 39);
            this.lbx_authors.Name = "lbx_authors";
            this.lbx_authors.Size = new System.Drawing.Size(438, 580);
            this.lbx_authors.TabIndex = 19;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(13F, 32F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1473, 881);
            this.Controls.Add(this.admin_tabControl);
            this.Name = "MainForm";
            this.Text = "BookMark Admin";
            this.admin_tabControl.ResumeLayout(false);
            this.tab_dashboard.ResumeLayout(false);
            this.tab_dashboard.PerformLayout();
            this.tab_books.ResumeLayout(false);
            this.tab_books.PerformLayout();
            this.tab_readers.ResumeLayout(false);
            this.tab_readers.PerformLayout();
            this.tab_authors.ResumeLayout(false);
            this.tab_authors.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private TabControl admin_tabControl;
        private TabPage tab_dashboard;
        private TabPage tab_books;
        private TabPage tab_readers;
        private TabPage tab_authors;
        private Label label1;
        private Label label2;
        private Label lbl_users;
        private Label lbl_booksweek;
        private Label lbl_books;
        private Label lbl_auth_read;
        private Button btn_viewComment;
        private Label lbl_bookComments;
        private ListBox lbx_bookComments;
        private Label lbl_bookPages;
        private Label lbl_bookGenre;
        private Label lbl_bookRating;
        private RichTextBox rtxt_bookDescription;
        private Label label6;
        private Label lbl_bookDate;
        private TextBox txt_bookIsbn;
        private Label lbl_bookIsbn;
        private Label lbl_bookTitle;
        private TextBox txt_bookTitle;
        private Button btn_bookDetails;
        private Button btn_viewBooks;
        private ListBox lbx_books;
        private Button btn_refreshStats;
        private Button btn_viewReaders;
        private ListBox lbx_readers;
        private Button btn_readerDetails;
        private TextBox txt_readerCountry;
        private Label lbl_readerCountry;
        private Label lbl_readerName;
        private TextBox txt_readerName;
        private TextBox txt_readerEmail;
        private Label lbl_readerEmail;
        private TextBox txt_readerPreferredNumberOfPages;
        private Label lbl_readerPreferredNumberOfPages;
        private ListBox lbx_readerPreferredGenres;
        private Label lbl_readerPreferredGenres;
        private TextBox txt_readerId;
        private Label lbl_readerId;
        private ListBox lbx_authorBooks;
        private Label lbl_authorBooks;
        private TextBox txt_authorId;
        private Label lbl_authorId;
        private TextBox txt_authorNumberOfBooks;
        private Label lbl_authorNumberOfBooks;
        private TextBox txt_authorEmail;
        private Label lbl_authorEmail;
        private TextBox txt_authorCountry;
        private Label lbl_authorCountry;
        private Label lbl_authorName;
        private TextBox txt_authorName;
        private Button btn_viewAuthorDetails;
        private Button btn_viewAuthors;
        private ListBox lbx_authors;
    }
}