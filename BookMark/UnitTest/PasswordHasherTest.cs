﻿using DomainClassLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTest
{
    [TestClass]
    public class PasswordHasherTest
    {
        [TestMethod]
        public void HashPassword_Test()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());
            PasswordManager passwordManager = new PasswordManager();
            string testPassword = "hopethetestsucceeds";
            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", testPassword, "IT", 1);

            // Act
            bookMark.AddUser(author);


            // Assert
            Assert.IsTrue(passwordManager.ValidatePassword(testPassword, bookMark.GetUser(1).Password));
        }
    }
}
