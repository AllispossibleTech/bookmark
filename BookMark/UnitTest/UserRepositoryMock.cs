﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DomainClassLibrary;

namespace UnitTest
{
    public class UserRepositoryMock : IUserRepository
    {
        private List<User> users;
        private List<Book> books;

        public UserRepositoryMock()
        {
            this.users = new List<User>();
            this.books = new List<Book>();
        }

        public User GetUser(int id)
        {
            foreach(User u in this.users)
            {
                if (u.Id == id)
                {
                    return u;
                }
            }
            return null;
        }

        public List<Reader> GetReaders()
        {
            List<Reader> result = new List<Reader>();
            foreach(User u in this.users)
            {
                if (u is Reader)
                {
                    result.Add((Reader)u);
                }
            }
            return result;
        }

        public List<Book> GetReadBooksByReaderId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Genre> GetPreferredGenresByReaderId(int id)
        {
            throw new NotImplementedException();
        }

        public List<Comment> GetCommentsByIsbn(string isbn)
        {
            throw new NotImplementedException();
        }

        public List<Book> GetBooksByAuthor(Author author)
        {
            throw new NotImplementedException();
        }

        public List<Author> GetAuthors()
        {
            List<Author> result = new List<Author>();
            foreach (User u in this.users)
            {
                if (u is Author)
                {
                    result.Add((Author)u);
                }
                
            }
            return result;
        }

        public List<User> GetAll()
        {
            List<User> result = new List<User>();

            foreach(Author a in this.GetAuthors())
            {
                result.Add(a);
            }
            foreach (Reader r in this.GetReaders())
            {
                result.Add(r);
            }
            return result;
        }

        public Author GetAuthorById(int id)
        {
            throw new NotImplementedException();
        }

        public Reader GetReader(int id)
        {
            throw new NotImplementedException();
        }

        public void AddBook(Book b)
        {
            this.books.Add(b);
            foreach (User u in this.users)
            {
                if (u is Author)
                {
                    if (u.Id == b.Author.Id)
                    {
                        ((Author)u).AddBook(b);
                    }
                }
            }
        }

        public void AddVote(int id, int voteNr)
        {
            foreach(User u in this.users)
            {
                if (u is Reader)
                {
                    if (u.Id == id)
                    {
                        ((Reader)u).AddVote();
                        break;
                    }
                }
            }
        }

        public void AddRating(Book b)
        {
            foreach(Book book in this.books)
            {
                if (b.Isbn == book.Isbn)
                {
                    this.books.Remove(book);
                    this.books.Add(b);
                    break;
                }
            }
        }

        public bool Exists(int id)
        {
            throw new NotImplementedException();
        }

        public void Add(User user)
        {
            this.users.Add(user);
        }

        public void DeleteUser(int id)
        {
            throw new NotImplementedException();
        }

        public void AddComment(Comment c, string isbn)
        {
            foreach (Book book in this.books)
            {
                if (isbn == book.Isbn)
                {
                    book.AddComment(c);
                }
            }
            
        }

    }
}
