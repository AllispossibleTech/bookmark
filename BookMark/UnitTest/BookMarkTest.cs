using DomainClassLibrary;

namespace UnitTest
{
    [TestClass]
    public class BookMarkTest
    {
        [TestMethod]
        public void AddAndGetUserIdTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());
            Author author1 = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            // Act
            bool result1 = bookMark.AddUser(author1);
            int result2 = bookMark.GetUserId(author1.Email, author1.Password);

            // Assert
            Assert.IsTrue(result1);
            Assert.AreEqual(author1.Id, result2);
        }

        [TestMethod]
        public void GetUserNoMatchingPasswordTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());
            Author author1 = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            // Act
            int result = bookMark.GetUserId(author1.Email, "WRONGPASSWORD");

            // Assert
            Assert.AreEqual(-1, result);
        }

        [TestMethod]
        public void AddUserNoUserProvidedTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());

            // Act
            bool result = bookMark.AddUser(null);

            // Assert
            Assert.IsFalse(result);
        }

        [TestMethod]
        public void AddAndGetBooksTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());
            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);
            Book book = new Book("9375630583621", "Test Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 300);
            List<Book> books = new List<Book>();
            books.Add(book);

            // Act
            bookMark.AddUser(author);
            bookMark.AddBook(book);
            List<Book> result = bookMark.GetBooks();

            // Assert
            Assert.AreEqual(books.Count, result.Count);
        }

        [TestMethod]
        public void GetPopularBooksTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());
            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);
            Book popularBook = new Book("9375630583621", "Popular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 300, 5, 1);
            Book unPopularBook = new Book("0475926483956", "Unpopular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 300, 2, 1);
            
            List<Book> popularBooks = new List<Book>();
            popularBooks.Add(popularBook);

            // Act
            bookMark.AddUser(author);
            bookMark.AddBook(popularBook);
            bookMark.AddBook(unPopularBook);
            List<Book> result = bookMark.GetPopularBooks();

            // Assert
            Assert.AreEqual(popularBooks.Count, result.Count);
        }

        [TestMethod]
        public void AddRatingTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());

            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            List<Genre> lovedGenres = new List<Genre>();
            lovedGenres.Add(Genre.Horror);
            Reader reader = new Reader("Gino", "Panino", "gino@gmail.com", "pws", "IT", lovedGenres, 300);
            Reader reader2 = new Reader("Lorenzo", "Panini", "lollo@gmail.com", "spw", "IT", lovedGenres, 300);

            Book book = new Book("9375630583621", "Popular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 300, 0, 0);

            // Act
            bookMark.AddUser(author);
            bookMark.AddUser(reader);
            bookMark.AddBook(book);
            bookMark.AddRating(5, book.Isbn, reader.Id);
            bookMark.AddRating(1, book.Isbn, reader2.Id);
            double result = bookMark.GetBook(book.Isbn).GetRating();

            // Assert
            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void GetSuggestedBooksTest()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());

            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            List<Genre> lovedGenres = new List<Genre>();
            lovedGenres.Add(Genre.Mystery);
            Reader reader = new Reader("Gino", "Panino", "gino@gmail.com", "pws", "IT", lovedGenres, 300);

            Book book = new Book("9375630583621", "Popular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 312, 0, 0);

            // Act
            bookMark.AddUser(author);
            bookMark.AddUser(reader);
            bookMark.AddBook(book);
            Book result = bookMark.GetSuggestedBooks(reader)[0];

            // Assert
            Assert.AreEqual(book, result);
        }

        [TestMethod]
        public void GetSuggestedBooks_GenreNotMatching_Test()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());

            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            List<Genre> lovedGenres = new List<Genre>();
            lovedGenres.Add(Genre.Fantasy);
            Reader reader = new Reader("Gino", "Panino", "gino@gmail.com", "pws", "IT", lovedGenres, 300);

            Book book = new Book("9375630583621", "Popular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 312, 0, 0);

            // Act
            bookMark.AddUser(author);
            bookMark.AddUser(reader);
            bookMark.AddBook(book);
            int result = bookMark.GetSuggestedBooks(reader).Count;

            // Assert
            Assert.AreEqual(0, result);
        }

        [TestMethod]
        public void GetSuggestedBooks_PreferredPageNumberNotMatching_Test()
        {
            // Arrange
            BookMark bookMark = new BookMark(new UserRepositoryMock());

            Author author = new Author("Alessandro", "Mattiuzzo", "ale@gmail.com", "psw", "IT", 1);

            List<Genre> lovedGenres = new List<Genre>();
            lovedGenres.Add(Genre.Mystery);
            Reader reader = new Reader("Gino", "Panino", "gino@gmail.com", "pws", "IT", lovedGenres, 400);

            Book book = new Book("9375630583621", "Popular Book", author, "Hope the test succeeds!", DateTime.Now, Genre.Mystery, 312, 0, 0);

            // Act
            bookMark.AddUser(author);
            bookMark.AddUser(reader);
            bookMark.AddBook(book);
            int result = bookMark.GetSuggestedBooks(reader).Count;

            // Assert
            Assert.AreEqual(0, result);
        }


    }
}