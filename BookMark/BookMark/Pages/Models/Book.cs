﻿using DomainClassLibrary;
using System.ComponentModel.DataAnnotations;

namespace BookMark.Pages.Models
{
    public class Book
    {
        [Required(ErrorMessage = "Please fill in the International Standard Book Number (ISBN): 13 digit number")]
        public string Isbn { get; set; }


        [Required(ErrorMessage = "Please fill in the Book Title")]
        public string Title { get; set; }
        

        [Required(ErrorMessage = "Please fill in the Book Description")]
        public string Description { get; set; }
        

        [Required(ErrorMessage = "Please fill in the number of pages the book has")]
        public int PageNumber { get; set; }


        [Required(ErrorMessage = "Please select the book's genre")]
        public Genre Genre { get; set; }
        

        [Required(ErrorMessage = "Please fill in the book's publish date")]
        public DateTime PublishDate { get; set; }
        

    }
}
