﻿using System.ComponentModel.DataAnnotations;

namespace BookMark.Pages.Models
{
    public class Comment
    {
        [Required(ErrorMessage = "You cannot add a blank comment!")]
        public string CommentText { get; set; }
    }
}
