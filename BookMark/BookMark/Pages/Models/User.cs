﻿using System.ComponentModel.DataAnnotations;

namespace BookMark.Pages.Models
{
    public class User
    {
        [Required(ErrorMessage = "Please fill in your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please fill in your surname")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Please fill in your email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Please fill in your password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Please select your country")]
        public string Country { get; set; }
        

        public int Id { get; set; }
    }
}
