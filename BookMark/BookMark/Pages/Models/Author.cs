﻿namespace BookMark.Pages.Models
{
    public class Author : Models.User
    {
        public List<Models.Book> PublishedBooks { get; set; }
    }
}
