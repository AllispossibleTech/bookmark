﻿using Microsoft.AspNetCore.Authorization;
using System.ComponentModel.DataAnnotations;

namespace BookMark.Pages.Models
{
    [Authorize]
    public class Search
    {
        [Required(ErrorMessage = "Input something to search!")]
        public string Query { get; set; }
        public bool Books { get; set; }
        public bool Authors { get; set; }


    }
}
