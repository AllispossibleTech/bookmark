﻿using DomainClassLibrary;
using System.ComponentModel.DataAnnotations;

namespace BookMark.Pages.Models
{
    public class Reader : User
    {

        [Required(ErrorMessage = "Please specify your preferred number of pages for a book")]
        public int PreferredNumberOfPages { get; set; }

    }
}
