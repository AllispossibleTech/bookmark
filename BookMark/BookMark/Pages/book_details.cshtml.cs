using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using Microsoft.AspNetCore.Authorization;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize]
    public class book_detailsModel : PageModel
    {
        public Book Book { get; set; }
        
        [BindProperty]
        public Models.Comment Comment { get; set; }

    public void OnGet(string? isbn)
        {
            if (isbn != "")
            {
                DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
                this.Book = bookMark.GetBook(isbn);
            }
            else
            {
                
            }
        }

        public IActionResult OnPost(string isbn)
        {
            if (ModelState.IsValid && User.IsInRole("Reader"))
            {
                DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
                Reader r = (Reader)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value)); 

                bookMark.AddComment(new Comment(r, this.Comment.CommentText, DateTime.Now), bookMark.GetBook(isbn));
            }
            return RedirectToPage("/book_details", new { isbn = isbn});
        }
    }
}

    
