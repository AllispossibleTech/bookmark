﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
namespace BookMark.Pages;
using DataAccessClassLibrary;

public class IndexModel : PageModel
{
    private readonly ILogger<IndexModel> _logger;
    private List<Models.Book> books = new List<Models.Book>();
    public IReadOnlyList<Models.Book> Books
    {
        get { return this.books.AsReadOnly(); }
    }


    public IndexModel(ILogger<IndexModel> logger)
    {
        _logger = logger;
    }

    public void OnGet()
    {
        DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
        bookMark.GetBooks().ForEach(b => this.books.Add(new Models.Book() { Title = b.Title, Isbn = b.Isbn}));
        // Implemented only title & isbn properties because they are the only needed to show the slide of books
    }
}

