﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using DomainClassLibrary;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize(Roles = "Author")]
    public class addBookModel : PageModel
    {
        [BindProperty]
        public Models.Book Book { get; set; }

        public void OnGet()
        {

        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {

                DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
                bookMark.AddBook(new Book(this.Book.Isbn, this.Book.Title, ((Author)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value))), this.Book.Description, this.Book.PublishDate, this.Book.Genre, this.Book.PageNumber));

                return new RedirectToPageResult("/author_home");
            }
            return Page();
        }
    }
}
