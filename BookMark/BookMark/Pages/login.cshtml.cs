﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Data;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
	public class loginModel : PageModel
    {
        [BindProperty]
        public Models.Credentials Credentials { get; set; }
        
        public void OnGet()
        {
            if (Request.Cookies.ContainsKey("RememberMeCookieName"))
            {
                this.Credentials = new Models.Credentials();
                this.Credentials.Username = Request.Cookies["RememberMeCookieName"];
                this.Credentials.RememberMe = true;
            }
        }

        public IActionResult OnPost()
        {
            DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
            int userId = bookMark.GetUserId(Credentials.Username, Credentials.Password);
            if (ModelState.IsValid)
            {
                if (userId != -1)
                {
                    List<Claim> claims = new List<Claim>();
                    claims.Add(new Claim("id", userId.ToString()));
                    claims.Add(new Claim(ClaimTypes.Email, Credentials.Username));
                    claims.Add(new Claim(ClaimTypes.Name, bookMark.GetUser(userId).FullName));
                    claims.Add(new Claim(ClaimTypes.Country, bookMark.GetUser(userId).Country));

                    if (bookMark.GetUser(userId) is Reader)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, "Reader"));
                    } 
                    else if (bookMark.GetUser(userId) is Author)
                    {
                        claims.Add(new Claim(ClaimTypes.Role, "Author"));
                    }

                    var claimsIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
                    HttpContext.SignInAsync(new ClaimsPrincipal(claimsIdentity));

                    if (this.Credentials.RememberMe)
                    {
                        CookieOptions cookieOptions = new CookieOptions();
                        cookieOptions.Expires = DateTime.Now.AddDays(1);
                        Response.Cookies.Append("RememberMeCookieName", this.Credentials.Username, cookieOptions);
                    }

                    if (User.IsInRole("Reader"))
                    {
                        return new RedirectToPageResult("reader_home");
                    } 
                    else if (User.IsInRole("Author"))
                    {
                        return new RedirectToPageResult("author_home");
                    }

                    return new RedirectToPageResult("profile");
                }
            }
            return Page();
        }
    }
}
