﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using Microsoft.AspNetCore.Authorization;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize(Roles = "Reader")]
    public class reader_homeModel : PageModel
    {
        public List<Book> Suggested { get; set; }
        public List<Book> Read { get; set; } 
        public List<Book> Popular { get; set; }

        public void OnGet()
        {
            DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
            Reader reader = ((Reader)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value)));

            Suggested = new List<Book>();
            Read = new List<Book>();
            Popular = new List<Book>();

            Suggested = bookMark.GetSuggestedBooks(reader);
            Read = reader.ReadBooks;
            Popular = bookMark.GetPopularBooks();
        }
    }
}
