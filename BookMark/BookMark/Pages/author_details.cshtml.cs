using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using Microsoft.AspNetCore.Authorization;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize]
    public class author_detailsModel : PageModel
    {
        public Author Author { get; set; }
        public void OnGet(int? id)
        {
            if (id.HasValue)
            {
                DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
                this.Author = (Author)bookMark.GetUser(Convert.ToInt32(id));
            }
        }
    }
}
