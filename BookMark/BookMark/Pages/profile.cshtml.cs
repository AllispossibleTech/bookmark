using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using Microsoft.AspNetCore.Authorization;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize]
    public class profileModel : PageModel
    {
        public Author Author { get; set; }
        public Reader Reader { get; set; }

        public void OnGet()
        {
            // ToDo: add way to enable users to change their data
            DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());

            if (User.IsInRole("Reader"))
            {
                this.Reader = (Reader)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value));
            }
            else if (User.IsInRole("Author"))
            {
                this.Author = (Author)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value));
            }
        }
    }
}
