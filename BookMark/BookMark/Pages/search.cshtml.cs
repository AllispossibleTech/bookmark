using DataAccessClassLibrary;
using DomainClassLibrary;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Linq.Expressions;

namespace BookMark.Pages
{
    public class searchModel : PageModel
    {
        [BindProperty]
        public Models.Search Search { get; set; }

        public List<Author> Authors { get; set; } = new List<Author>();
        public List<Book> Books { get; set; } = new List<Book>();
        public bool Searching { get; set; } = false;


        public void OnGet(string? query)
        {
            DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());

            if (query != null)
            {
                foreach (User u in bookMark.GetUsers())
                {
                    if (u is Author)
                    {
                        if (u.FullName.Contains(query, System.StringComparison.CurrentCultureIgnoreCase))
                        {
                            this.Searching = true;
                            this.Authors.Add((Author)u);
                        }
                    }
                }
                foreach (Author a in this.Authors)
                {
                    foreach (Book b in a.PublishedBooks)
                    {
                        this.Books.Add(b);
                    }
                }
                

                foreach (Book b in bookMark.GetBooks())
                {
                    if (b.Title.Contains(query, System.StringComparison.CurrentCultureIgnoreCase) || b.Isbn.Contains(query))
                    {
                        this.Searching = true;
                        this.Books.Add(b);
                    }
                }
            }
        }

        public void OnPost()
        {
            if (ModelState.IsValid)
            {
                Response.Redirect($"/search?query={this.Search.Query}");
            }
            Page();
            
        }
    }
}
