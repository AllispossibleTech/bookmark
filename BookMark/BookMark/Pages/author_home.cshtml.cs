using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using DataAccessClassLibrary;

namespace BookMark.Pages
{
    [Authorize(Roles = "Author")]
    public class author_homeModel : PageModel
    {
        public List<Book> Published { get; set; } = new();
        public void OnGet(int? id)
        {
            //if(id.HasValue)
            //{
            DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());
            foreach (Book b in ((Author)bookMark.GetUser(Convert.ToInt32(User.FindFirst("id").Value))).PublishedBooks)
            {
                Published.Add(b);
            }

            //}
        }
    }
}
