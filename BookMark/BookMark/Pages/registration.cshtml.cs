﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using DomainClassLibrary;
using DataAccessClassLibrary;
namespace BookMark.Pages
{
	public class registrationModel : PageModel
    {

        [BindProperty]
        public Models.Reader Reader { get; set; }
        [BindProperty]
        public bool LiteraryFiction { get; set; }
        [BindProperty]
        public bool Mystery { get; set; }
        [BindProperty]
        public bool Thriller { get; set; }
        [BindProperty]
        public bool Horror { get; set; }
        [BindProperty]
        public bool Historical { get; set; }
        [BindProperty]
        public bool Romance { get; set; }
        [BindProperty]
        public bool Western { get; set; }
        [BindProperty]
        public bool ScienceFiction { get; set; }
        [BindProperty]
        public bool Fantasy { get; set; }
        [BindProperty]
        public bool Dystopian { get; set; }
        [BindProperty]
        public bool MagicalRealism { get; set; }
        [BindProperty]
        public bool RealistLiterature { get; set; }
        public void OnGet(int? id)
        {
            // if (id.HasValue)
            // {
            // }
        }

        public IActionResult OnPost()
        {
            if (ModelState.IsValid)
            {
                List<Genre> lovedGenres = new List<Genre>();
                if (this.LiteraryFiction)
                {
                    lovedGenres.Add(Genre.Literary_Fiction);
                }
                if (this.Mystery)
                {
                    lovedGenres.Add(Genre.Mystery);
                }
                if (this.Thriller)
                {
                    lovedGenres.Add(Genre.Thriller);
                }
                if (this.Horror)
                {
                    lovedGenres.Add(Genre.Horror);
                }
                if (this.Historical)
                {
                    lovedGenres.Add(Genre.Historical);
                }
                if (this.Romance)
                {
                    lovedGenres.Add(Genre.Romance);
                }
                if (this.Western)
                {
                    lovedGenres.Add(Genre.Western);
                }
                if (this.ScienceFiction)
                {
                    lovedGenres.Add(Genre.Science_Fiction);
                }
                if (this.Fantasy)
                {
                    lovedGenres.Add(Genre.Fantasy);
                }
                if (this.Dystopian)
                {
                    lovedGenres.Add(Genre.Dystopian);
                }
                if (this.MagicalRealism)
                {
                    lovedGenres.Add(Genre.Magical_Realism);
                }
                if (this.RealistLiterature)
                {
                    lovedGenres.Add(Genre.Realist_Literature);
                }
                DomainClassLibrary.BookMark bookMark = new DomainClassLibrary.BookMark(new UserRepository());

                // HASHING
                // PasswordManager passwordManager = new PasswordManager();
                // string hashed = passwordManager.HashPassword(this.Reader.Password);

                bookMark.AddUser(new DomainClassLibrary.Reader(this.Reader.Name, this.Reader.Surname, this.Reader.Email, this.Reader.Password, this.Reader.Country, lovedGenres, this.Reader.PreferredNumberOfPages));
                
                // ToDo: Redirect user to homepage after sign up with user data
                return new RedirectToPageResult("/reader_home");
            }
            return Page();
        }
    }
}
